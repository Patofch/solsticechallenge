package com.pato.fernandez.solsticechallenge.providers;

import android.content.Context;

import com.pato.fernandez.solsticechallenge.adapters.RetrofitAdapter;
import com.pato.fernandez.solsticechallenge.entidades.Contacto;
import com.pato.fernandez.solsticechallenge.interfaces.ContactosClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Patricio Fernández Challen on 28/02/2019.
 */

public class ContactosProvider {

    private Context context;
    private OnContactosLoaded listenerContactos;
    private ArrayList<Contacto> contactos;
    private static ContactosProvider instance;

//  Implemento un Singleton para utilizar siempre la misma instancia del objeto

    public static ContactosProvider getInstance(Context context){
        if(instance == null){
            instance = new ContactosProvider(context);
        }
        return instance;
    }

    private ContactosProvider(Context context) {
        this.context = context;
        contactos = new ArrayList<>();
    }

    //    Primero evalua el flag reload, si esta en true, va a buscar los contactos y los carga en el arreglo;
    //    si esta en false y el arreglo está vacío tambien va a cargarlos; sino devuelve el arreglo de contactos ya cargado anteriormente
    public ArrayList<Contacto> getContactos(boolean reload){
        if(!reload && !contactos.isEmpty()){
            return contactos;
        }
        Retrofit retrofit = new RetrofitAdapter().getAdapter();
        ContactosClient peliculasClient = retrofit.create(ContactosClient.class);
        Call<ArrayList<Contacto>> call = peliculasClient.getContactos();
        call.enqueue(new Callback<ArrayList<Contacto>>() {
            @Override
            public void onResponse(Call<ArrayList<Contacto>> call, Response<ArrayList<Contacto>> response) {
                if(listenerContactos != null){
                    contactos = response.body();
                    listenerContactos.onLoaded(response.body());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Contacto>> call, Throwable t) {
                if(listenerContactos != null){
                    listenerContactos.onLoadedFailure(t);
                }
            }
        });
        return contactos;
    }

    public Contacto getContacto(String id){
        for (Contacto c : contactos) {
            if(c.getId().equals(id)) {
                return c;
            }
        }
        return null;
    }

    public void setOnContactosLoadedListener(OnContactosLoaded listener) {
        this.listenerContactos = listener;
    }

    //    Interfaz para la notificación de la carga de contactos
    public interface OnContactosLoaded {
        public void onLoaded(ArrayList<Contacto> contactos);
        public void onLoadedFailure(Throwable t);
    }

}