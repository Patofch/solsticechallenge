package com.pato.fernandez.solsticechallenge.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.pato.fernandez.solsticechallenge.R;
import com.pato.fernandez.solsticechallenge.adapters.ContactosAdapter;
import com.pato.fernandez.solsticechallenge.entidades.Contacto;
import com.pato.fernandez.solsticechallenge.providers.ContactosProvider;

import java.util.ArrayList;

/**
 * Created by Patricio Fernández Challen on 28/02/2019.
 */

public class ContactsListActivity extends AppCompatActivity implements ContactosProvider.OnContactosLoaded {

    private ListView lv_contactos;
    private ContactosProvider contactosProvider;
    private ContactosAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_list);
        contactosProvider = ContactosProvider.getInstance(this);
        contactosProvider.setOnContactosLoadedListener(this);
        adapter = new ContactosAdapter(this);
        adapter.setContactos(contactosProvider.getContactos(false));
        lv_contactos = findViewById(R.id.lv_contactos);
        lv_contactos.setAdapter(adapter);
    }

    @Override
    public void onLoaded(ArrayList<Contacto> contactos) {
        adapter.setContactos(contactos);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadedFailure(Throwable t) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        adapter.sortContactos();
        adapter.notifyDataSetChanged();
    }
}
