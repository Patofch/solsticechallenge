package com.pato.fernandez.solsticechallenge.adapters;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Patricio Fernández Challen on 28/02/2019.
 */

public class RetrofitAdapter {

    Retrofit retrofit;

    public RetrofitAdapter(){
    }

    public Retrofit getAdapter(){
        retrofit = new Retrofit.Builder()
                .baseUrl("https://s3.amazonaws.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }
}