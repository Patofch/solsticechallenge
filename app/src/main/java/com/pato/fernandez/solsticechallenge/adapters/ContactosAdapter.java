package com.pato.fernandez.solsticechallenge.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pato.fernandez.solsticechallenge.R;
import com.pato.fernandez.solsticechallenge.activities.ContactoActivity;
import com.pato.fernandez.solsticechallenge.activities.ContactsListActivity;
import com.pato.fernandez.solsticechallenge.entidades.Contacto;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Patricio Fernández Challen on 28/02/2019.
 */

public class ContactosAdapter  extends BaseAdapter {

    private Context context;
    private ArrayList<Contacto> contactos;
    private ArrayList<Contacto> contactosList;

    public ContactosAdapter(Context context) {
        this.context = context;
    }

    public void setContactos(ArrayList<Contacto> contactos) {
        this.contactos = contactos;
        sortContactos();
    }

    public void sortContactos(){
        ArrayList<Contacto> contactosFav = new ArrayList<>();
        ArrayList<Contacto> contactosOther = new ArrayList<>();
        for (int i = 0; i < contactos.size(); i++) {
            Contacto c = contactos.get(i);
            if(c.getIsFavorite()) {
                contactosFav.add(c);
            }
            else {
                contactosOther.add(c);
            }
        }
        Collections.sort(contactosFav, new Comparator<Contacto>(){
            public int compare(Contacto obj1, Contacto obj2) {
                return obj1.getName().compareToIgnoreCase(obj2.getName());
            }
        });
        Collections.sort(contactosOther, new Comparator<Contacto>(){
            public int compare(Contacto obj1, Contacto obj2) {
                return obj1.getName().compareToIgnoreCase(obj2.getName());
            }
        });
        contactosList = new ArrayList<>();
        Contacto favoritos = new Contacto();
        favoritos.setName("Favorite Contacts");
        favoritos.setCompanyName("HEADER");
        Contacto others = new Contacto();
        others.setName("Other Contacts");
        others.setCompanyName("HEADER");
        if(!contactosFav.isEmpty()) {
            contactosList.add(favoritos);
        }
        contactosList.addAll(contactosFav);
        if(!contactosOther.isEmpty()){
            contactosList.add(others);
        }
        contactosList.addAll(contactosOther);
    }

    @Override
    public int getCount() {
        return contactosList.size();
    }

    @Override
    public Object getItem(int position) {
        return contactosList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.card_contacto, parent, false);
        final Contacto contacto = contactosList.get(position);
        if(contacto.getCompanyName()!= null && contacto.getCompanyName().equals("HEADER")){
            v = inflater.inflate(R.layout.header, parent, false);
            ((TextView)v.findViewById(R.id.tv_header)).setText(contacto.getName());
            return v;
        }
        if(!contacto.getIsFavorite()){
            v.findViewById(R.id.iv_favorito).setVisibility(View.GONE);
        }

        ((TextView)v.findViewById(R.id.tv_nombre)).setText(contacto.getName());
        ((TextView)v.findViewById(R.id.tv_company)).setText(contacto.getCompanyName());

        Picasso.get().load(contacto.getSmallImageURL()).into((ImageView)v.findViewById(R.id.iv_contacto));
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ContactoActivity.class);
                intent.putExtra("id", contacto.getId());
                ((ContactsListActivity)context).startActivityForResult(intent, 0);
            }
        });
        return v;
    }

}
