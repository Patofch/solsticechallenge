package com.pato.fernandez.solsticechallenge.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pato.fernandez.solsticechallenge.R;
import com.pato.fernandez.solsticechallenge.entidades.Contacto;
import com.pato.fernandez.solsticechallenge.providers.ContactosProvider;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Locale;

/**
 * Created by Patricio Fernández Challen on 28/02/2019.
 */

public class ContactoActivity extends AppCompatActivity {

    private ContactosProvider contactosProvider;
    private Contacto contacto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto);
        contactosProvider = ContactosProvider.getInstance(this);
        Intent intent = getIntent();
        // cargo el id del contacto seleccionado para poder mostrar el detalle
        String id= "";
        if(intent != null){
            id = intent.getStringExtra("id");
        }
        if(!id.equals("")){
            cargarContacto(id);
        }
    }

    private void cargarContacto(String id){
        contacto = contactosProvider.getContacto(id);

        findViewById(R.id.tv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactoActivity.this.finish();
            }
        });

        actualizarFavorito(contacto.getIsFavorite());
        findViewById(R.id.iv_favorito).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contacto.setIsFavorite(!contacto.getIsFavorite());
                actualizarFavorito(contacto.getIsFavorite());
            }
        });

        Picasso.get().load(contacto.getLargeImageURL()).into((ImageView)findViewById(R.id.iv_contacto));

        ((TextView)findViewById(R.id.tv_phone_home)).setText(contacto.getPhone().getHome());
        ((TextView)findViewById(R.id.tv_phone_mobile)).setText(contacto.getPhone().getMobile());
        ((TextView)findViewById(R.id.tv_phone_work)).setText(contacto.getPhone().getWork());
        ((TextView)findViewById(R.id.tv_nombre)).setText(contacto.getName());
        ((TextView)findViewById(R.id.tv_company)).setText(contacto.getCompanyName());
        ((TextView)findViewById(R.id.tv_address)).setText(contacto.getAddress().getStreet());

        ((TextView)findViewById(R.id.tv_birthdate)).setText(formatearFecha(contacto.getBirthdate()));
        ((TextView)findViewById(R.id.tv_email)).setText(contacto.getEmailAddress());
        ((TextView)findViewById(R.id.tv_city)).setText(contacto.getAddress().getCity() + ", " +
                contacto.getAddress().getState() + " " + contacto.getAddress().getZipCode() + ", " +
                contacto.getAddress().getCountry());

        //Valida si los campos tienen información que mostrar, sino los oculto
        if(contacto.getPhone().getHome() == null || contacto.getPhone().getHome().isEmpty()){
            findViewById(R.id.contenedor_home).setVisibility(View.GONE);
        }
        if(contacto.getPhone().getWork() == null || contacto.getPhone().getWork().isEmpty()){
            findViewById(R.id.contenedor_work).setVisibility(View.GONE);
        }
        if(contacto.getPhone().getMobile() == null || contacto.getPhone().getMobile().isEmpty()){
            findViewById(R.id.contenedor_mobile).setVisibility(View.GONE);
        }
        if(contacto.getAddress() == null || contacto.getAddress().getStreet().isEmpty()){
            findViewById(R.id.contenedor_address).setVisibility(View.GONE);
        }
        if(contacto.getBirthdate() == null || contacto.getBirthdate().isEmpty()){
            findViewById(R.id.contenedor_birthday).setVisibility(View.GONE);
        }
        if(contacto.getEmailAddress() == null || contacto.getEmailAddress().isEmpty()){
            findViewById(R.id.contenedor_email).setVisibility(View.GONE);
        }
    }

    private String formatearFecha(String fecha){
        fecha = fecha.replace("-", "/");
        return DateFormat.getDateInstance(DateFormat.LONG, Locale.US).format(new java.util.Date(fecha));
    }

    private void actualizarFavorito(boolean isFavorito){
        if(isFavorito){
            ((ImageView)findViewById(R.id.iv_favorito)).setImageResource(R.mipmap.ic_favorite_true);
        }else{
            ((ImageView)findViewById(R.id.iv_favorito)).setImageResource(R.mipmap.ic_favorite_false);
        }
    }

}
