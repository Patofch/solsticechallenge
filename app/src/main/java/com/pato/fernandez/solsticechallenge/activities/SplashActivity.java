package com.pato.fernandez.solsticechallenge.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pato.fernandez.solsticechallenge.R;
import com.pato.fernandez.solsticechallenge.entidades.Contacto;
import com.pato.fernandez.solsticechallenge.providers.ContactosProvider;

import java.util.ArrayList;

/**
 * Created by Patricio Fernández Challen on 28/02/2019.
 */

public class SplashActivity  extends AppCompatActivity implements ContactosProvider.OnContactosLoaded {

    private final int SPLASH_DISPLAY_LENGTH = 25;
    private ProgressBar progressBar;
    private TextView tv_progress;
    private ContactosProvider contactosProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        progressBar = findViewById(R.id.progressBar);
        tv_progress = findViewById(R.id.tv_progress);
        contactosProvider = ContactosProvider.getInstance(SplashActivity.this);
//        contactosProvider.setOnContactosLoadedListener(this);
//        contactosProvider.getContactos(true);
        new AsyncTaskContactos().execute();
    }

    @Override
    public void onLoaded(ArrayList<Contacto> contactos) {
        new AsyncTaskContactos().execute();
    }

    @Override
    public void onLoadedFailure(Throwable t) {

    }

    private class AsyncTaskContactos extends AsyncTask<Void, String, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (int i = 1; i < 100; i++) {
                try {
                    Thread.sleep(SPLASH_DISPLAY_LENGTH);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(String.valueOf(i));
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            if(values[0].equals("0")){
                tv_progress.setText(values[1]);
            }
            else {
                tv_progress.setText(values[0] + "%");
                progressBar.setProgress(Integer.valueOf(values[0]));
            }
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent mainIntent = new Intent(SplashActivity.this, ContactsListActivity.class);
            SplashActivity.this.startActivity(mainIntent);
            SplashActivity.this.finish();
        }
    }
}