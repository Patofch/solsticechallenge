package com.pato.fernandez.solsticechallenge.interfaces;

import com.pato.fernandez.solsticechallenge.entidades.Contacto;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Patricio Fernández Challen on 28/02/2019.
 */

public interface ContactosClient {
    @GET("technical-challenge/v3/contacts.json")
    Call<ArrayList<Contacto>> getContactos();
}
